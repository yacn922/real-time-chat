<?php

namespace Modules\Users\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Modules\Users\Entities\Colleges;
use Modules\Users\Entities\Friends;
use Modules\Users\Entities\User;

class FriendsController extends Controller
{
    public function __construct()
    {
        $this->middleware('JWT');
    }

    public function getFriends($status)
    {
        $friends = Friends::GetFriends($status);

        return response($friends);
    }

    public function addNewFriend(Request $request)
    {
        $username = $request->username;
        $user = User::firstWhere('username', $username);

        $friend = new Friends();
        $friend->user_id_1 = $user->id;
        $friend->user_id_2 = Auth::id();
        $friend->status = 'notApproved';
        $friend->save();
    }

    public function changeFriendStatus(Request $request, $id)
    {
        $status = Friends::firstWhere('id', $id);
        $status->status = $request->status;
        $status->save();
    }

    public function getCollegeOperator($id)
    {
        return User::where([
            ['college_id', $id],
            ['type', 'operator']
        ])->get();
    }

    public function addNewCollege(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $college = Colleges::create(array_merge(
            $validator->validated()
        ));

        return response()->json([
            'message' => 'College successfully registered',
            'value' => $college,
        ], 201);
    }

}

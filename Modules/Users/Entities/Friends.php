<?php

namespace Modules\Users\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Friends extends Model
{
    use HasFactory;

    protected $fillable = [];

    protected static function newFactory()
    {
        return \Modules\Users\Database\factories\FriendsFactory::new();
    }

    public function user1() {
        return $this->belongsTo(User::class, 'user_id_1');
    }

    public function user2() {
        return $this->belongsTo(User::class, 'user_id_2');
    }

    public function scopeGetFriends($query, $status)
    {
        switch ($status) {
            case 'all':
                $query->where('user_id_1', auth()->id())
                ->orWhere('user_id_2', auth()->id());
                break;
            case 'requests':
                $query->where([
                    ['status', 'notApproved'],
                    ['user_id_1', auth()->id()]
                ]);
                break;
            case 'notApproved':
                $query->where([
                    ['status', 'notApproved'],
                    ['user_id_2', auth()->id()]
                ]);
                break;
            case 'approved' :
                $query->where([
                    ['status', 'approved'],
                    ['user_id_1', auth()->id()]
                ])->orWhere([
                    ['status', 'approved'],
                    ['user_id_2', auth()->id()]
                ]);
                break;
            case 'ignored' :
                $query->where([
                    ['status', 'ignored'],
                    ['user_id_1', auth()->id()]
                ]);
                break;
        }

        return $query->with(['user1', 'user2'])->latest()->get();
    }
}

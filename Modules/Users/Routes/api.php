<?php

use Modules\Users\Entities\Colleges;

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::get('getFriends/{status}', 'FriendsController@getFriends');
Route::post('sendFriendReq', 'FriendsController@addNewFriend');
Route::post('changeFriendStatus/{id}', 'FriendsController@changeFriendStatus');
Route::get('allColleges', function () {
    return Colleges::all();
});
Route::post('addNewCollege', 'FriendsController@addNewCollege');
Route::get('operators/{id}', 'FriendsController@getCollegeOperator');

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('friends', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id_1');
            $table->foreign('user_id_1')->on('users')->references('id')->cascadeOnDelete();
            $table->unsignedBigInteger('user_id_2');
            $table->foreign('user_id_2')->on('users')->references('id')->cascadeOnDelete();
            $table->enum('status', ['approved', 'notApproved', 'ignored']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('friends');
    }
}

export default {
    getData() {
        this.$store.dispatch('conversations/setMainLoadingStatus', true)
        this.$store.dispatch('friends/setMainLoadingStatus', true)
        Promise.all([
            axios.get('getFriends/approved'),
            axios.get('getConversations/all')
        ])
            .then(axios.spread((friends, conversations) => {
                this.$store.dispatch('conversations/updateConversationList', {
                    type: 'all',
                    data: conversations.data
                })
                this.$store.dispatch('friends/updateFriendList', {
                    type: 'approved',
                    data: friends.data
                })

                this.$store.dispatch('conversations/setMainLoadingStatus', false)
                this.$store.dispatch('friends/setMainLoadingStatus', false)
            }))
            .catch(err => {
                this.$store.dispatch('conversations/setMainLoadingStatus', false)
                this.$store.dispatch('friends/setMainLoadingStatus', false)
            })
    },
    changeType(ui) {
        this.$store.dispatch('app/setType', ui)
    },
}

export const namespaced = true

export const state = {
    loggedIn: false,
    user: {}
}

export const mutations = {
    SET_USER(state, user) {
        state.loggedIn = true
        state.user = user
    },
    REMOVE_USER(state) {
        localStorage.removeItem('YaCn')
        state.loggedIn = false
        localStorage.removeAll()

    }
}

export const actions = {
    setUser({commit}, user) {
        commit('SET_USER', user)
    },
    removeUser({commit}) {
        commit('REMOVE_USER')
    }
}

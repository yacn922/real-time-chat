export const namespaced = true

export const state = {
    list: {
        approved: [],
        notApproved: [],
        all: [],
        activeLists: [],
        requests: []
    },
    sendRequestDialog: {
        dialogStatus: false
    },
    disableFriendList: false,
    friendLoading: false
}

export const mutations = {
    SET_MAIN_LOADING_STATUS(state, status) {
        state.friendLoading = status
    },
    SET_SEND_DIALOG_STATUS(state, status) {

    },
    REMOVE_SELECTED_FRIEND(state, friend) {

    },
    UPDATE_FRIEND_LIST(state, payload) {
        switch (payload.type) {
            case 'approved':
                state.list.approved = payload.data
                break;
            case 'notApproved':
                state.list.notApproved = payload.data
                break;
            case 'all':
                state.list.all = payload.data
                break;
            case 'requests':
                state.list.requests = payload.data
                break;
        }
        state.list.activeLists = payload.data
    }
}

export const actions = {
    setMainLoadingStatus({commit}, status) {
        commit('SET_MAIN_LOADING_STATUS', status)
    },
    setSendDialogStatus({commit}, status) {

    },
    removeSelectedFriend({commit}, friend) {

    },
    updateFriendList({commit}, payload) {
        commit('UPDATE_FRIEND_LIST', payload)
    },
}

export const namespaced = true

export const state = {
    conversationDialog: {
        dialogStatus: false,
        loadingStatus: false,
        conversationChanel: '',
        conversationData: null
    },
    list: {
        private: [],
        group: [],
        all: [],
        activeLists: []
    },
    groupDialog: {
        dialogStatus: false,
        loadingStatus: false
    },
    conversationLoading: false
}

export const mutations = {
    SHOW_CONVERSATION_DIALOG(state, conversation) {

    },
    CLOSE_CONVERSATION_DIALOG(state) {

    },
    DELETE_CONVERSATION_DIALOG(state) {

    },
    PUSH_NEW_MESSAGE(state, message) {

    },
    PUSH_NEW_CONVERSATION(state, conversation) {

    },
    SET_CREATE_GROUP_CONVERSATION_DIALOG(state, status) {

    },
    SET_MAIN_LOADING_STATUS(state, status) {
        state.conversationLoading = status
    },
    UPDATE_CONVERSATION_LIST(state,  payload) {
        switch (payload.type) {
            case 'private':
                state.list.private = payload.data
                break;
            case 'group':
                state.list.group = payload.data
                break;
            case 'all':
                state.list.all = payload.data
                break;
        }
        state.list.activeLists = payload.data
    },
    SET_CONVERSATION_LOADING_STATUS(state, status) {

    },
}

export const actions ={
    showConversationDialog({commit}, conversation) {

    },
    setCreateGroupConversationDialog({commit}, status) {

    },
    setMainLoadingStatus({commit}, status) {
        commit('SET_MAIN_LOADING_STATUS', status)
    },
    setConversationLoadingStatus({commit}, status) {

    },
    updateConversationList({commit}, payload) {
        commit('UPDATE_CONVERSATION_LIST', payload)
    },
    closeConversationDialog({commit}) {

    },
    deleteConversationDialog({commit}) {

    },
    pushNewMessage({commit}, message) {

    },
    pushNewConversation({commit}, conversation) {

    },
}

export const namespaced = true

export const state = {
    ui: {
        defaultUi: 0,
        defaultType: 0
    }
}

export const mutations = {
    SET_UI(state, ui) {
        state.ui.defaultUi = ui
    },
    SET_TYPE(state, type) {
        state.ui.defaultType = type
    }
}

export const actions = {
    setUi({commit}, ui) {
        commit('SET_UI', ui)
    },
    setType({commit}, type) {
        commit('SET_TYPE', type)
    }
}

import Vue from "vue";
import Vuex from "vuex";

import createPersistedState from 'vuex-persistedstate'
import secureLs from 'secure-ls'

import * as user from './modules/user'
import * as app from './modules/app'
import * as conversations from './modules/conversations'
import * as friends from './modules/friends'

Vue.use(Vuex)

const ls = new secureLs( {isCompression: false} )

export default new Vuex.Store(
    {
        modules: {
            user,
            app,
            conversations,
            friends
        },
        state: {

        },
        plugins: [
            createPersistedState({
                key: 'YaCn',
                path: ['user'],
                storage: {
                    getItem: (key) => ls.get(key),
                    setItem: (key, value) => ls.set(key, value),
                    removeItem: (key) => ls.remove(key)
                }
            })
        ]
    }
)

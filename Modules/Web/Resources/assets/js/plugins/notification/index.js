const msgOptions = {
    timeout: 0,
    showProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    position: 'leftTop',
    bodyMaxLength: 500,
    titleMaxLength: 100
}

export default {
    install (Vue) {
        Vue.mixin({
            methods: {
                $showSimple (body, title) {
                    this.$snotify.warning(body, title, msgOptions)
                },
                $showSuccess (body, title) {
                    this.$snotify.success(body, title, msgOptions)
                },
                $showError (body, title) {
                    this.$snotify.error(body, title, msgOptions)
                },
                $showInfo (body, title) {
                    this.$snotify.info(body, title, msgOptions)
                },
                $showWarning (body, title) {
                    this.$snotify.warning(body, title, msgOptions)
                },
                $showConfirm (id,msg) {
                    id ? document.getElementById(`removeBtn-${id}`,msg).blur() : ''
                    return new Promise ((resolve,reject) => {
                        let toast = this.$snotify.confirm(
                            msg,
                            this.$t('shared.Warning'),
                            {
                                backdrop: 0.8,
                                position: 'centerCenter',
                                buttons: [
                                    {
                                        text: this.$t('shared.Yes'),
                                        className: 'bg-success',
                                        action: () => {
                                            this.$snotify.remove(toast.id)
                                            toast.on('hidden', (toast) => toast.config.backdrop = -1 )
                                            resolve()
                                        },
                                        bold: true
                                    },
                                    {
                                        text: this.$t('shared.No'),
                                        className: 'bg-danger',
                                        action: () => {
                                            this.$snotify.remove(toast.id)
                                            reject()
                                        },
                                        bold: true
                                    }
                                ]
                            }
                        )
                    })
                },
                $getLocaleErrorMessage (err, prefix) {
                    if (err.status === 401 && err.data.details !== 'loginFailed') {
                        return this.$t('shared.unauthorized')
                    }

                    if (err.status === 500) {
                        const msg = (err.data && err.data.Exception && err.data.Exception.Message) || this.$t('shared.internalServerError')
                        return msg
                    }

                    const msg = err.data && (err.data.details || err.data.code)
                    if (msg) {
                        if (prefix && this.$te(prefix + '.' + msg)) {
                            return this.$t(prefix + '.' + msg)
                        } else if (this.$te('shared.' + msg)) {
                            return this.$t('shared.' + msg)
                        }
                        return msg
                    } else {
                        return err.message || err.Message || err
                    }
                }
            }
        })
    }
}


require('./bootstrap');

import Vue from 'vue';

window.Vue = require('vue').default;

import store from "./plugins/store/store";

//plugins

import notification from './plugins/notification'
import vuetify from './plugins/vuetify';
import vuex from './plugins/vuex'

import Snotify from 'vue-snotify'

Vue.use(Snotify)
Vue.use(notification)

import App from './app.vue'


const app = new Vue({
    el: '#app',
    vuetify,
    store,
    render: h => h(App)
});


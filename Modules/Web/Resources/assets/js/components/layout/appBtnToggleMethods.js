export default {
    btnAction(action, subject) {
        if (subject === 1) {
            this.$emit('getFriends', action)
        } else if (subject === 2) {
            this.$emit('getConversations', action)
        }
    },

}

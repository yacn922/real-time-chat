import appConversationsBarMethods from "../../conversations/components/appConversationsBarMethods";

export default {
    getFriends(type) {
        this.$store.dispatch('friends/setMainLoadingStatus', true)
        axios.get(`getFriends/${type}`)
            .then(res => {
                this.$store.dispatch('friends/setMainLoadingStatus', false)
                this.$store.dispatch('friends/updateFriendList', {
                    type: type,
                    data: res.data
                })
            })
            .catch(err => {
                this.$store.dispatch('friends/setMainLoadingStatus', false)
            })
    },
    sendRequest() {
        axios.post(`sendFriendReq`, {
            username: this.username
        })
            .then(res => {
                this.getFriends(this.active)
                this.addFriendDialog = false
                this.username = ''
            })
            .catch(err => {
                console.log(err)
            })
    },
    changeStatus(id, status) {
        axios.post(`changeFriendStatus/${id}`, {
            status: status
        })
            .then(res => {
                this.getFriends(this.active)
            })
            .catch(err => {
                console.log(err)
            })
    },
    deleteFriend() {

    },
    startConversation(user1, user2) {
        axios.post(`newConversation`, {
            user_id_1: user1,
            user_id_2: user2
        })
            .then(res => {
                this.getConversations(this.currentChat)
            })
            .catch(err => {
                console.log(err)
            })
    },

}

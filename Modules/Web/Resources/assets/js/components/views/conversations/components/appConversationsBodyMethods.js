export default {
    connect() {
        let vm = this;
        this.getMessages(this.chatBox.id);
        window.Echo.private('chat')
            .listen('.message.new', e => {
                vm.getMessages(e.chatMessage.conversation_id)
                vm.getConversations()
            })
        /*if( this.chatBox.id ) {

        }*/
    },
    getConversations() {
        this.$store.dispatch('conversations/setMainLoadingStatus', true)
        axios.get(`getConversations/all`)
            .then(res => {
                this.$store.dispatch('conversations/setMainLoadingStatus', false)
                this.$store.dispatch('conversations/updateConversationList', {
                    type: 'all',
                    data: res.data
                })
            })
            .catch(err => {
                this.$store.dispatch('conversations/setMainLoadingStatus', false)
            })
    },
    getMessages(chatBox) {
        axios.get(`getMessages/${chatBox}`)
            .then(res => {
                this.currentConversationMessage = res.data
                this.chatTab = 2;
                if(res.data && res.data[(res.data.length) - 1].is_read === 0
                    && res.data[(res.data.length) - 1].writer_id !== this.user.user.user.id) {
                    this.$showWarning(`شما یک پیام جدید از سمت ${res.data[(res.data.length) - 1].writer.name} دارید!`, 'پیام جدید!')
                }
                window.scrollTo(0,document.body.scrollHeight);
            })
    },
    getChatHistory() {
        axios.get('getConversations/all')
            .then(res => {
                this.chatTab = 1
                this.allChats = res.data
            })
    },
    startChat(i) {
        this.dialog = true
        this.chatBox = this.conversations[i]
        this.seenMessage()
    },
    seenMessage() {
        axios.put(`seenMessage/${this.chatBox.id}`)
            .then(res => {
                console.log(res)
            })
    },
    close() {
        this.$emit('update:dialog', false)
    },
    sendMessage() {
        axios.post(`sendMessage/${this.chatBox.id}`, {
            message: this.message
        })
            .then(res => {
                if (res.status === 201) {
                    this.message = ''
                    this.getMessages(this.chatBox.id)
                }
            })
            .cache(err => {
                console.log(err)
            })

    },
    showSendRequestDialog() {

    }
}

export default {
    getConversations(type) {
        this.$store.dispatch('conversations/setMainLoadingStatus', true)
        axios.get(`getConversations/${type}`)
            .then(res => {
                this.$store.dispatch('conversations/setMainLoadingStatus', false)
                this.$store.dispatch('conversations/updateConversationList', {
                    type: type,
                    data: res.data
                })
            })
            .catch(err => {
                this.$store.dispatch('conversations/setMainLoadingStatus', false)
            })
    },
    startConversation(username) {
        axios.post(`newConversation`, {
            username: username
        })
            .then(res => {
                this.addConversation = false
                this.getConversations(this.active)
            })
            .catch(err => {
                console.log(err)
            })
    },
    conversationConnect() {
        if( this.loggedUser.id ) {
            let vm = this;
            this.getConversations('all');
            window.Echo.private('conversation.' +this.loggedUser.id )
                .listen('.conversation.new', e => {
                    vm.getConversations('all')
                })
        }
    },
}

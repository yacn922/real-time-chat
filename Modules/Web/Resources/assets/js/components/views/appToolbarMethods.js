export default {
    loginAuth() {
        this.loadingBtn = true
        axios.post('auth/login', this.userForm)
            .then(res => {
                this.$store.dispatch('user/setUser', res.data)
                    .then(() => {
                        this.menu = false
                        this.loadingBtn = false
                        location.reload();
                    })
            })
            .catch(err => {
                this.$showError('کاربری با مشخصات وارد شده یافت نشد!')
                this.loadingBtn = false
                console.log(err)
            })
    },
    addUser() {
        this.loadingBtn = true
        axios.post('auth/register', {
            username: this.userForm.username,
            name: this.userForm.name,
            password: this.userForm.password,
            password_confirmation: this.userForm.password_confirmation,
            type: this.userForm.type,
            college_id: this.userForm.college_id.id
        })
            .then(res => {
                this.menu = false
                this.loadingBtn = false
                this.alert = true

                if (res.status === 201) {
                    this.$showSuccess('اپراتور با موفقیت اضافه شد!')
                    this.userForm.username = ''
                    this.userForm.name = ''
                    this.userForm.password = ''
                    this.userForm.password_confirmation = ''
                    this.userForm.college_id = ''
                }
            })
            .catch(err => {
                this.loadingBtn = false
                this.$showError('مشکلی به وجود آمده داده های وارد شده را مجددا چک کنید!')
                console.log(err)
            })
    },

    changeUi(ui) {
        this.$store.dispatch('app/setUi', ui)
    },
    getColleges() {
        axios.get('allColleges')
            .then(res => {
                for (let i = 0; i < res.data.length; i++) {
                    this.colleges = res.data
                }
            })
            .catch(err => {
                console.log(err)
            })
    },

    addCollege() {
        this.loadingBtn = true
        axios.post('addNewCollege', {
            name: this.name,
        })
            .then(res => {
                this.menu = false
                this.loadingBtn = false
                this.alert = true

                if (res.status === 201) {
                    this.$showSuccess('دانشکده با موفقیت اضافه شد!')
                    this.name = ''
                    this.getColleges()
                }
            })
            .catch(err => {
                this.loadingBtn = false
                this.$showError('مشکلی به وجود آمده داده های وارد شده را مجددا چک کنید!')
                console.log(err)
            })
    },
}

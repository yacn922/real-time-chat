<?php

namespace Modules\Messages\Http\Controllers;

use App\Event\NewChatMessage;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Messages\Entities\Messages;

class MessagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('JWT');
    }

    public function getMessages($chat_id)
    {
        $messages = Messages::GetMessages($chat_id);

        return response($messages);
    }

    public function sendMessage(Request $request, $chatId)
    {
        $message = new Messages();
        $message->writer_id = Auth::id();
        $message->conversation_id = $chatId;
        $message->message = $request->message;
        $message->save();

        broadcast(new NewChatMessage( $message ))->toOthers();

        return response()->json([
            $message
        ], 201);
    }

    public function seenMessage(Request $request, $chat_id)
    {
         Messages::where([
            ['conversation_id', $chat_id],
            ['writer_id', '!=', Auth::id()]
            ])->update(['is_read' => 1]);

    }

}

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/messages', function (Request $request) {
    return $request->user();
});*/

Route::get('getMessages/{chat_id}', 'MessagesController@getMessages');
Route::post('sendMessage/{chat_id}', 'MessagesController@sendMessage');
Route::put('seenMessage/{chat_id}', 'MessagesController@seenMessage');

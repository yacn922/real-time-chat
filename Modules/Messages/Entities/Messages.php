<?php

namespace Modules\Messages\Entities;

use App\Event\NewChatMessage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Auth;
use Modules\Conversations\Entities\Conversations;
use Modules\Users\Entities\User;

class Messages extends Model
{
    use HasFactory;

    protected $fillable = [];

    protected static function newFactory()
    {
        return \Modules\Messages\Database\factories\MessagesFactory::new();
    }

    public function writer()
    {
        return $this->belongsTo(User::class, 'writer_id');
    }

    public function scopeGetMessages($query, $chat_id)
    {
        $query->where('conversation_id', $chat_id);

        return $query->with(['writer'])
            ->orderBy('created_at', 'ASC')
            ->get();
    }


}

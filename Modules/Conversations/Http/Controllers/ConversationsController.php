<?php

namespace Modules\Conversations\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Conversations\Entities\Conversations;
use Modules\Users\Entities\User;

class ConversationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('JWT');
    }

    public function getConversations($type)
    {
        $conversations = Conversations::GetConversations($type);

        return response($conversations);
    }

    public function newConversation(Request $request)
    {
        $conversation = new Conversations();

        if (!$request->username)
        {
            $query = Conversations::where([
                ['user_id_1', $request->user_id_1],
                ['user_id_2', $request->user_id_2]
            ])->orWhere([
                ['user_id_1', $request->user_id_2],
                ['user_id_2', $request->user_id_1]
            ])->get();

            if (count($query) === 0) {
                $conversation->user_id_1 = $request->user_id_1;
                $conversation->user_id_2 = $request->user_id_2;
                $conversation->type = 'private';
                $conversation->save();

                return response()->json([
                    'message' => 'متصل شدید',
                    'status' => 200,
                    'data' => $conversation
                ], 200);

            } else {
                return response()->json([
                    'message' => 'شما قبل متصل شده اید',
                    'status' => 400,
                    'data' => $query
                ], 400);
            }

        } else {
            $user = User::firstWhere('username', $request->username);
            $query = Conversations::where([
                ['user_id_1', auth()->id()],
                ['user_id_2', $user]
            ])->orWhere([
                ['user_id_1', $user],
                ['user_id_2', auth()->id()]
            ])->get();

            if (count($query) === 0) {
                $conversation->user_id_1 = auth()->id();
                $conversation->user_id_2 = $user->id;
                $conversation->type = 'private';
                $conversation->save();

                return response()->json([
                    'message' => 'متصل شدید',
                    'status' => 200,
                ], 200);

            } else {
                return response()->json([
                    'message' => 'شما قبل متصل شده اید',
                    'status' => 400
                ], 400);
            }
        }
    }
}

<?php

namespace Modules\Conversations\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Users\Entities\Colleges;
use Modules\Users\Entities\User;

class Conversations extends Model
{
    use HasFactory;

    protected $fillable = [];

    protected static function newFactory()
    {
        return \Modules\Conversations\Database\factories\ConversationsFactory::new();
    }


    public function user1() {
        return $this->belongsTo(User::class, 'user_id_1');
    }

    public function user2() {
        return $this->belongsTo(User::class, 'user_id_2');
    }

    public function members()
    {
        return $this->hasMany(ConversationsMembers::class, 'conversation_id');
    }

    public function scopeGetConversations($query, $type)
    {
        if ($type === 'all') {
            $query->where('user_id_1', auth()->id())
            ->orWhere('user_id_2', auth()->id())
            ->orWhereHas('members', function ($query) {
                $query->where('member_id', auth()->id());
            });
        } elseif ($type === 'private') {
            $query->where([
                ['user_id_1', auth()->id()],
                ['type', $type]
            ])->orWhere([
                ['user_id_2', auth()->id()],
                ['type', $type]
            ]);
        } elseif ($type === 'group') {
            $query->where('type', $type);
            $query->whereHas('members', function ($query) {
               $query->where('member_id', auth()->id());
            });
        }

        return $query->with(['user1', 'user2'])->latest()->get();
    }
}

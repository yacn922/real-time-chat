<?php

namespace Modules\Conversations\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ConversationsMembers extends Model
{
    use HasFactory;

    protected $fillable = [];
    
    protected static function newFactory()
    {
        return \Modules\Conversations\Database\factories\ConversationsMembersFactory::new();
    }
}

<?php

namespace App\Listeners;

use App\Event\NewConversation;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendNewConversationNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewConversation  $event
     * @return void
     */
    public function handle(NewConversation $event)
    {
        //
    }
}

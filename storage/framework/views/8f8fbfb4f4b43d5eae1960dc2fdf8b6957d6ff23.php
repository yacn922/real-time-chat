<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module Web</title>

       
        <link rel="stylesheet" href="<?php echo e(mix('css/web.css')); ?>">

    </head>
    <body>
        <?php echo $__env->yieldContent('content'); ?>

        
         <script src="<?php echo e(mix('js/web.js')); ?>"></script>
    </body>
</html>
<?php /**PATH D:\Project\shahrood\Realtime Chat\Modules/Web\Resources/views/layouts/master.blade.php ENDPATH**/ ?>